package cn.hetra.test;

import cn.hetra.hj212.core.HJ212Codec;
import cn.hetra.hj212.core.HJ212Data;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.junit.Test;
public class HJ212CodecTest {
    LoggingHandler loggingHandler = new LoggingHandler(LogLevel.WARN);

    @Test
    public void test() throws InterruptedException {
        HJ212Data parser = new HJ212Data.Parser("QN=20230203000000058;ST=27;CN=2031;PW=123456;MN=20230201;Flag=6;PNUM=2;PNO=1;CP=&&DataTime=20230202000000;a0000-Cou=-0.00,a0000-Min=-0.00,a0000-Avg=-0.00,a0000-Max=0.00,a0000-Flag=N;a0001-Cou=33000.00,a0001-Min=100.00,a0001-Avg=100.00,a0001-Max=100.00,a0001-Flag=N;a0002-Cou=66000.00,a0002-Min=-0.00,a0002-Avg=174.60,a0002-Max=200.00,a0002-Flag=N;a0003-Cou=99000.00,a0003-Min=300.00,a0003-Avg=300.00,a0003-Max=300.00,a0003-Flag=N;a0004-Cou=132000.00,a0004-Min=400.00,a0004-Avg=400.00,a0004-Max=400.00,a0004-Flag=N;a0005-Cou=165000.00,a0005-Min=500.00,a0005-Avg=500.00,a0005-Max=500.00,a0005-Flag=N;a0006-Cou=198000.00,a0006-Min=600.00,a0006-Avg=600.00,a0006-Max=600.00,a0006-Flag=N;a0007-Cou=231000.00,a0007-Min=700.00,a0007-Avg=700.00,a0007-Max=700.00,a0007-Flag=N;a0008-Cou=264000.00,a0008-Min=800.00,a0008-Avg=800.00,a0008-Max=800.00,a0008-Flag=N;a0009-Cou=297000.00,a0009-Min=900.00,a0009-Avg=900.00,a0009-Max=900.00,a0009-Flag=N&&");
        EmbeddedChannel client = new EmbeddedChannel (new StringEncoder(),loggingHandler,new HJ212Codec());
        client.writeOutbound(parser);
        EmbeddedChannel server = new EmbeddedChannel (new LineBasedFrameDecoder(1024)
                , new StringDecoder(),loggingHandler,new HJ212Codec());
        ByteBuf o = client.readOutbound();
        while(null!= o){
            ByteBuf byteBuf = Unpooled.copiedBuffer(o);
            server.writeInbound(byteBuf);
            o = client.readOutbound();
        }
        HJ212Data o1 = server.readInbound();
        System.out.println(o1.getData());
        server.finish();
        client.finish();
    }
}
