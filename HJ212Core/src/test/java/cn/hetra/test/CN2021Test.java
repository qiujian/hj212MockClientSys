package cn.hetra.test;

import cn.hetra.hj212.core.CPS;
import cn.hetra.hj212.core.HJ212Data;
import org.joda.time.DateTime;
import org.junit.Test;

public class CN2021Test {

    @Test
    public void test(){
        HJ212Data build = new HJ212Data.Builder()
                .QN(new DateTime().toString("YYYYMMDDHHmmssSSS"))
                .ST("32")
                .CN("2021")
                .PW("100000")
                .MN("010000A8900016F000169DC0")
                .version(1)
                .A(1)
                .D(0)
                .Cps(CPS.create(""))
                .build();
        System.out.println(build.getData());
    }


}
