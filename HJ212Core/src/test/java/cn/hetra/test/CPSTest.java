package cn.hetra.test;
import cn.hetra.hj212.core.CPS;
import com.alibaba.fastjson.JSON;
import org.junit.Test;

public class CPSTest {


    @Test
    public void test004(){
        CPS cps = CPS.create("");
        cps.new Parser().parse();
    }


    @Test
    public void test01(){

        CPS.Builder builder = new CPS.Builder();
        builder.addEntry("a","1");
        builder.addEntry("b","2");
        builder.addEntry("b","2");
//        builder.addGroupEntry(entry->{
//            entry.addEntry("a1-1","1");
//            entry.addEntry("a2-1","2");
//        });
        System.out.println(builder.build());
        CPS.CPSGroups cpsGroups = CPS.create(builder.build().toString()).new Parser().parseGroup("1");
        System.out.println(JSON.toJSONString(cpsGroups));

    }




    @Test
    public void test(){
        CPS.CPSGroups parse = CPS.create("QnRtn=1").new Parser().parse();
        System.out.println(JSON.toJSONString(parse));

    }
}
