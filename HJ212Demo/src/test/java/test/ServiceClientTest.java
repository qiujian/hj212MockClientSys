package test;

import cn.hetra.hj212.client.HJ212ClientSession;
import cn.hetra.hj212.client.HJ212TcpClient;
import cn.hetra.hj212.core.CPS;
import cn.hetra.hj212.core.HJ212Codec;
import cn.hetra.hj212.core.HJ212Data;
import com.google.common.io.Resources;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.logging.LoggingHandler;
import org.junit.jupiter.api.Test;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.netty.tcp.TcpClient;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

public class ServiceClientTest {
    /**
     * 模拟链接成功后发送报文
     */
    @Test
    public void test0001(){
        HJ212TcpClient hj212TcpClient = new HJ212TcpClient("10.237.124.186", 8085);
        HJ212ClientSession hj212ClientSession = new HJ212ClientSession(){
            @Override
            public void handleMessage(HJ212Data message) {
                HJ212Data.Builder res = new HJ212Data.Builder(message.clone());
                send(res.A(0).CN("9011").clearCP()
                                .Cps(CPS.create("QnRtn=1"))
                                .build());
                System.out.println("Server returned: " + message);
            }
        };
        ListenableFuture<Void> connect = hj212TcpClient.connect(hj212ClientSession);
        Mono.create(sink->{
            connect.addCallback(new ListenableFutureCallback(){
                @Override
                public void onSuccess(Object result) {
                    new Timer().scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            try {
                                testRaw().subscribe(data->{
                                    hj212ClientSession.send(data);
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    },5000,1000);
                }
                @Override
                public void onFailure(Throwable ex) {
                    ex.printStackTrace();
                }
            });
        }).block();
    }

    @Test
    public void test001() throws IOException {
        List<HJ212Data> block = testRaw().doOnNext(e -> {
                    System.out.println(e);
                }
        ).collectList().block();
        System.out.println(block.size());

    }
    public Flux<HJ212Data> testRaw() throws IOException {
        URL resource = Resources.getResource("upload.txt");
        InputStream inputStream = resource.openConnection().getInputStream();
        byte[] buf =  new byte[25];
        int len;
        EmbeddedChannel client = new EmbeddedChannel (new LineBasedFrameDecoder(1024)
                , new StringDecoder() , new StringEncoder(),new HJ212Codec());
        while((len = inputStream.read(buf))>0){
            ByteBuf byteBuf = Unpooled.copiedBuffer(buf,0,len);
            client.writeInbound(byteBuf);
        }
        client.flushInbound();
        client.finish();
        return Flux.fromIterable(client.inboundMessages())
                .map(HJ212Data.class::cast);
    }

}
