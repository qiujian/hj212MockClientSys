# 提供基于HJ212协议的污染源在线模拟终端系统

#### Description
提供基于HJ212协议的污染源在线模拟终端系统，系统实现了HJ212协议（污染物在线监控监测系统数据传输标准）的模拟设备终端。生态环境监测等相关开发从业者可以在该系统创建指定监控因子的监控设备，指定连接上位机的ip和端口，用于实时数据或历史数据的调试。污染源监控因子的数据生成支持设定数据曲线、异常率等来更贴近真实设备。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
