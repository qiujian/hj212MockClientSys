package cn.hetra.hj212.client;

import cn.hetra.hj212.core.HJ212Data;

public interface TcpConnectionHandler {
    /**
     * Invoked after a connection is successfully established.
     * @param connection the connection
     */
    void afterConnected(TcpConnection connection);
    /**
     * Invoked on failure to connect.
     * @param ex the exception
     */
    void afterConnectFailure(Throwable ex);
    /**
     * Handle a message received from the remote host.
     * @param message the message
     */
    void handleMessage(HJ212Data message);
    /**
     * Handle a failure on the connection.
     * @param ex the exception
     */
    void handleFailure(Throwable ex);

    /**
     * Invoked after the connection is closed.
     */
    void afterConnectionClosed();
}
